fx_version 'adamant'
game 'gta5'

description 'FiveM Lua Menu Framework'

files {
	'warmenu.lua',
	'warmenu_demo.lua'
}


lua54 'yes'
escrow_ignore {
   'html/*',
   'config.lua'	 
}